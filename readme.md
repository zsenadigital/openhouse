## Openhouse - Frontend Style Guide documentation

---

readme.md use this: https://www.markdownguide.org/basic-syntax/

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).


## FE setup // clone after

Make sure you have [Node.js](https://nodejs.org/en/download/) installed before setting up

1. npm install : project root
1. npm run dev : project build and url

## Atomic Design

Help: https://bradfrost.com/blog/post/atomic-web-design/#atoms

Put simply, atomic design just asserts that like organic matter, 
design systems can be broken down into their smallest pieces and built up from there. 
In web development, this means we shift from the mentality of building “pages” to breaking
down designs into patterns, organized smallest to largest, and use these building-block
patterns to develop the project. Here are the categories commonly used in this approach:

1. Atoms: simple HTML tags (e.g., 
```<button>, <input />, </button> <h1>, <a>, </a></h1>)```
2. Molecules: small combinations of atoms (search form, menu, lists)
3. Organisms: groups of molecules forming a distinct design section (header, masthead, footer)
4. Templates: organisms combined to form contextualized designs
5. Pages: fully actualized templates often with real content
---

## new page, routing

The [HtmlWebpackPlugin](https://webpack.js.org/plugins/html-webpack-plugin/) simplifies creation of HTML files to serve your webpack bundles. 
This is especially useful for webpack bundles that include a hash in the filename 
which changes every compilation. You can either let the plugin generate an HTML 
file for you, supply your own template using lodash templates, or use your own loader.

- webpack config js file contain this, example: 

```
   new HtmlWebpackPlugin({
      meta: meta,
      template: './src/hbs/templates/index.hbs',
      title: 'Home'
    }),
```
so if you need a new page then copy this function in a webpack.config.js