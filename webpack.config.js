const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const PreloadWebpackPlugin = require('preload-webpack-plugin');


let meta = {
  viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no'
};

module.exports = {
  devServer: {
    host: require('os').hostname().toLowerCase(),
    open: true
  },
  devtool: 'source-map',
  entry: './src/app.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  mode: 'development',
  module: {
    rules: [{
        test: /.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.(s?css)$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true,
              plugins: function () {
                return [
                  require('precss'),
                  require('autoprefixer')
                ];
              }
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true
            }
          }
        ]
      },
      {
        test: /\.(png|jp(e*)g|svg|gif)$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[path][name].[ext]',
            outputPath: 'img/'
          }
        }]
      },
      {
        test: /\.mp4$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[path][name].[ext]'
          }
        }]
      },
      {
        test: /\.hbs$/,
        use: [{
            loader: 'handlebars-loader',
            options: {
              helperDirs: path.resolve(__dirname, 'src/hbs/helper'),
              query: {
                inlineRequires: '\/img\/'
              },
              partialDirs: [
                path.join(__dirname, 'src/hbs/partials/atoms'),
                path.join(__dirname, 'src/hbs/partials/atoms/cards'),
                path.join(__dirname, 'src/hbs/partials/organisms'),
                path.join(__dirname, 'src/hbs/partials/organisms/sample_blocks'),
                path.join(__dirname, 'src/hbs/partials/molecules'),
                path.join(__dirname, 'src/hbs/partials/molecules/accordions'),
                path.join(__dirname, 'src/hbs/partials/molecules/collapses'),
                path.join(__dirname, 'src/hbs/partials/molecules/carousels'),
                path.join(__dirname, 'src/hbs/partials/molecules/forms'),
                path.join(__dirname, 'src/hbs/partials/molecules/nav'),
                path.join(__dirname, 'src/hbs/partials/molecules/modals'),
                path.join(__dirname, 'src/hbs/partials/pages/homepage'),
                path.join(__dirname, 'src/hbs/partials/pages/finances'),
                path.join(__dirname, 'src/hbs/partials/pages/franchise'),
                path.join(__dirname, 'src/hbs/partials/pages/list'),
                path.join(__dirname, 'src/hbs/partials/pages/details'),
                path.join(__dirname, 'src/hbs/partials/pages/offices'),
                path.join(__dirname, 'src/hbs/partials/pages/contact'),
                path.join(__dirname, 'src/hbs/partials/pages/sales'),
                path.join(__dirname, 'src/hbs/partials/pages/residential_park'),
                path.join(__dirname, 'src/hbs/templates/details'),
                path.join(__dirname, 'src/hbs/templates/details/_profile_details'),
                path.join(__dirname, 'src/hbs/partials/pages/csr'),
                path.join(__dirname, 'src/hbs/partials/pages/references'),
                path.join(__dirname, 'src/hbs/partials/pages/_oh_values'),
                path.join(__dirname, 'src/hbs/partials/pages/_about_us'),
                path.join(__dirname, 'src/hbs/partials/pages/services'),
                path.join(__dirname, 'src/hbs/partials/pages/career'),
                path.join(__dirname, 'src/hbs/templates'),
              ]
            }
          },
          {
            loader: 'extract-loader'
          },
          {
            loader: 'html-loader',
            options: {
              interpolate: true
            }
          }
        ]
      },
      {
        test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: '[name].[ext]',

          }
        }]
      }
    ]
  },
  plugins: [
    // must be on position 0 for overriding in webpack.typo3.config.js
    new CleanWebpackPlugin(['dist']),
    // must be on position 1 for overriding in webpack.typo3.config.js
    new MiniCssExtractPlugin({
      filename: 'app.[hash].css',
      chunkFilename: '[id].css'
    }),
    new HtmlWebpackPlugin({
      meta: meta,
      template: './src/hbs/templates/index.hbs',
      // filename: "/",
      title: 'Főoldal'
    }),
    new HtmlWebpackPlugin({
      meta: meta,
      template: './src/hbs/templates/services.hbs',
      filename: 'szolgaltatasok',
      title: 'Szolgáltatások'
    }),
    new PreloadWebpackPlugin({
      rel: 'preload',
      include: 'allChunks'
    }),
    new HtmlWebpackPlugin({
      meta: meta,
      template: './src/hbs/templates/finances.hbs',
      filename: 'penzugyek',
      title: 'Pénzügyek'
    }),
    new HtmlWebpackPlugin({
      meta: meta,
      template: './src/hbs/templates/offices.hbs',
      filename: 'irodaink',
      title: 'Irodáink'
    }),
    new HtmlWebpackPlugin({
      meta: meta,
      template: './src/hbs/templates/career.hbs',
      filename: 'karrier',
      title: 'Karrier'
    }),
    new HtmlWebpackPlugin({
      meta: meta,
      template: './src/hbs/templates/franchise.hbs',
      filename: 'franchise',
      title: 'Franchise'
    }),
    new HtmlWebpackPlugin({
      meta: meta,
      template: './src/hbs/templates/contact.hbs',
      filename: 'kapcsolat',
      title: 'Kapcsolat'
    }),
    new HtmlWebpackPlugin({
      meta: meta,
      template: './src/hbs/templates/demo.hbs',
      filename: 'demo',
      title: 'Frontend Style Guide - Komponensek bemutatása'
    }),
    new HtmlWebpackPlugin({
      meta: meta,
      template: './src/hbs/templates/_list.hbs',
      filename: 'listaoldal',
      title: 'Listaoldal'
    }),
    new HtmlWebpackPlugin({
      meta: meta,
      template: './src/hbs/templates/_list2.hbs',
      filename: 'listaoldal2',
      title: 'Listaoldal2'
    }),
    new HtmlWebpackPlugin({
      meta: meta,
      template: './src/hbs/templates/details/_office_details.hbs',
      filename: 'iroda-adatlap',
      title: 'Iroda adatlap'
    }),
    new HtmlWebpackPlugin({
      meta: meta,
      template: './src/hbs/templates/details/_sales_details.hbs',
      filename: 'ertekesito-adatlap',
      title: 'Érékesítő adatlap'
    }),
    new HtmlWebpackPlugin({
      meta: meta,
      template: './src/hbs/templates/details/_news_details_base.hbs',
      filename: 'hirek',
      title: 'Hírek'
    }),
    new HtmlWebpackPlugin({
      meta: meta,
      template: './src/hbs/templates/details/_news_details_single.hbs',
      filename: 'hirek-adatlap',
      title: 'Hírek adatlap'
    }),
    new HtmlWebpackPlugin({
      meta: meta,
      template: './src/hbs/templates/details/_profile_details.hbs',
      filename: 'profil-adatok',
      title: 'Profil adatok'
    }),
    new HtmlWebpackPlugin({
      meta: meta,
      template: './src/hbs/templates/_privacy.hbs',
      filename: 'szabalyzatok-es-tajekoztatok',
      title: 'Szabályzatok és tájékoztatók'
    }),
    new HtmlWebpackPlugin({
      meta: meta,
      template: './src/hbs/templates/_about_us.hbs',
      filename: 'rolunk',
      title: 'Rólunk'
    }),
    new HtmlWebpackPlugin({
      meta: meta,
      template: './src/hbs/templates/_csr.hbs',
      filename: 'csr',
      title: 'Csr'
    }),
    new HtmlWebpackPlugin({
      meta: meta,
      template: './src/hbs/templates/_oh_values.hbs',
      filename: 'oh-ertekek',
      title: 'Oh értekek'
    }),
    new HtmlWebpackPlugin({
      meta: meta,
      template: './src/hbs/templates/_references.hbs',
      filename: 'referenciak',
      title: 'Referenciák'
    }),

    new HtmlWebpackPlugin({
      meta: meta,
      template: './src/hbs/templates/details/real_estate_main.hbs',
      filename: 'ingatlan',
      title: 'Ingatlan'
    }),
    new HtmlWebpackPlugin({
      meta: meta,
      template: './src/hbs/templates/details/_real_estate_new.hbs',
      filename: 'uj-ingatlan',
      title: 'Új építésű ingatlan'
    }),

    new HtmlWebpackPlugin({
      meta: meta,
      template: './src/hbs/templates/details/residential_park.hbs',
      filename: 'lakopark',
      title: 'Lakópark'
    }),

    new HtmlWebpackPlugin({
      meta: meta,
      template: './src/hbs/templates/sales.hbs',
      filename: 'eladom-kiadom',
      title: 'Eladom / Kiadom'
    }),
    new HtmlWebpackPlugin({
      meta: meta,
      template: './src/hbs/templates/residential_park_main.hbs',
      filename: 'uj-epitesu',
      title: 'Lakoparkjaink'
    }),

    new HtmlWebpackPlugin({
      meta: meta,
      template: './src/hbs/templates/details/_career_details.hbs',
      filename: 'allas-adatlap',
      title: 'Állás adatlap'
    }),

    // new HtmlWebpackPlugin({
    //     meta: meta,
    //     template: './src/hbs/partials/pages/career/_collapse_details_office.hbs',
    //     filename: 'allas-adatlap-iroda',
    //     title: 'Állás adatlap iroda oldalról kattintva'
    // }),
    new CopyWebpackPlugin({
      patterns: [{
        from: 'src/img',
        to: 'img'
      }]
    })
  ]
};