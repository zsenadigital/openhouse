import "bootstrap";
import './scss/app.scss';

// import "@fortawesome/fontawesome-free/js/all.min";
import dot_slider from './js/dot_slider';
import slider_nav from './js/slider_nav';
import main from './js/main';
import 'slick-slider/slick/slick.min'
import 'slick-slider'

// import 'ekko-lightbox';
import 'lazysizes';
import "select2/dist/js/select2.full";
import "select2"

import forms from './js/form';

window.addEventListener("load", function () {
    main();
    dot_slider();
    slider_nav();
    forms();
});