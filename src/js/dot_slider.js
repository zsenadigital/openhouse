export default function () { }
window.jQuery = window.$ = require("jquery");

//spec offers
(function ($) {
    $(".o-slider-dots span").each(function (i, item) {
        item.onclick = function (event) {
            var _this = $(event.target);

            _this.parent().find("span").removeClass("active");
            _this.addClass("active");

            var imgs = _this.parent().parent().parent().find(".o-img-dot-slider img");

            imgs.addClass('d-none');
            imgs.eq(_this.prevAll().length).removeClass('d-none');
        }
    });
})(jQuery);