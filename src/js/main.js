export default function () {

window.jQuery = window.$ = require('jquery');

    window.changeSvg = function (e, _this) {
        e.preventDefault();
        e.stopPropagation();
        if($(_this).find('.layer_2').attr('fill') == '#B0191E'){
            $(_this).find('.layer_2').attr('fill', '#B2B2B2');
        } else {
            $(_this).find('.layer_2').attr('fill', '#B0191E');
        }
    }

$(document).ready(function() {

/*
    $('.collapse').collapse(
       'dispose'
    );

    */

    $('.o-primary-btn').on('click', function () {
        $(this).toggleClass('chevron-up');
    });

/*
 * jQuery Text Area Limit
 */

    $.fn.jQTArea = function (options) {
        var
            defaults =
                {
                    setLimit: 200, // Input limit
                    setExt: "W", // Animate Width (W) or Height (H) based on input length
                    setExtR: false // calculate setExt in reverse
                },
            plugin = $.extend({}, defaults, options);

        this.each(function () {
            var
                // get textarea element
                getTextarea = $(this);

            //set default values
            $(".jQTAreaValue").html(plugin.setLimit);
            $(".jQTAreaCount").html(0);

            // bind events to textarea
            if (getTextarea.is('textarea')) {
                getTextarea.bind("keyup focus change", function () {

                    // validate on event trigger
                    fnValidate($(this));
                });
            }

            function fnValidate(e) {
                var
                    // get input
                    calPerc,
                    getInput = e.val(),
                    // get input length
                    inputLength = getInput.length,
                    // get set limit
                    limit = plugin.setLimit;

                if (inputLength > limit) {
                    // truncate any input beyond the set limit
                    e.val(getInput.substring(0, limit));

                } else {
                    // set .jQTALimitCount to display input length
                    $(".jQTAreaCount").html(inputLength);

                    // set .jQTALimitCountR to display input length remaining
                    $(".jQTAreaCountR").html(limit - inputLength);

                    // set .jQTALimitExt to animate either width or height of element based on input length
                    calPerc = inputLength * 100 / limit;

                    //set setExt value to reverse eg, 80% instead of 20%
                    if (plugin.setExtR) {
                        calPerc = 100 - calPerc;
                    }

                    if (plugin.setExt === "W") {
                        $(".jQTAreaExt").width(
                            calPerc + "%"
                        )
                    } else {
                        $(".jQTAreaExt").height(
                            calPerc + "%"
                        )
                    }
                }
            }
        });

        return this;
    }

    // textarea counter animation
    $("form#baseContactFormS").find("textarea").jQTArea({
        setLimit: 256,
        setExt: "W",
        setExtR: true
    });

    // textarea counter animation
    $("form#baseContactFormC").find("textarea").jQTArea({
        setLimit: 256,
        setExt: "W",
        setExtR: true
    });

    // textarea counter animation
    $("form#baseContactForm").find("textarea").jQTArea({
        setLimit: 256,
        setExt: "W",
        setExtR: true
    });

    window.addEventListener(
        "scroll",
        () => {
            document.body.style.setProperty(
                "--scroll",
                window.pageYOffset / (document.body.offsetHeight - window.innerHeight)
            );
        },
        false
    );

    $(function() {
        var header = $(".start-style");
        $(window).scroll(function() {
            var scroll = $(window).scrollTop();

            if (scroll >= 10) {
                header.removeClass('start-style').addClass("scroll-on");
            } else {
                header.removeClass("scroll-on").addClass('start-style');
            }
        });
    });

    //homepage counter box

    $('.counter').each(function () {
        $(this).prop('Counter',0).animate({
            Counter: $(this).text()
        }, {
            duration: 4000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });


    //equal hight cards
    $(".o-carousel").each(function () {
        var highestBox = 0;

        $(".o-card.equal-card", this).each(function () {
            if ($(this).outerHeight(true) > highestBox) {
                highestBox = $(this).outerHeight(true);
            }
        });

        $(".o-card.equal-card", this).outerHeight(highestBox, true);
    });

    //Menu On Hover

    $('body').on('mouseenter mouseleave','.nav-item',function(e){
        if ($(window).width() > 750) {
            var _d=$(e.target).closest('.nav-item');_d.addClass('show');
            setTimeout(function(){
                _d[_d.is(':hover')?'addClass':'removeClass']('show');
            },1);
        }
    });


    // url location tabs
    let hash = window.location.hash;
    hash && $('ul.nav a[href="' + hash + '"]').tab('show');

    $('.nav-tabs a').click(function (e) {
        $(this).tab('show');
        window.location.hash = this.hash;
    });

    $('#profileDetails .nav-tabs a').click(function (e) {
        $(this).tab('show');
        var scrollmem = $('body').scrollTop();
        window.location.hash = this.hash;
        $('html,body').scrollTop(scrollmem);
    });



    //franchise counter card
    function animateValue(obj, start, end, duration) {
        var startTimestamp = null;

        var step = function step(timestamp) {
            if (!startTimestamp) startTimestamp = timestamp;
            var progress = Math.min((timestamp - startTimestamp) / duration, 1);

            if (obj) {
                obj.innerHTML = Math.floor(progress * (end - start) + start);
            }

            if (progress < 1) {
                window.requestAnimationFrame(step);
            }
        };

        window.requestAnimationFrame(step);
    }

    var obj = document.getElementById("value1");
    animateValue(obj, 0, 13, 1000);
    var obj2 = document.getElementById("value2");
    animateValue(obj2, 0, 32, 1000);
    var obj3 = document.getElementById("value3");
    animateValue(obj3, 0, 130, 1000);
    var obj4 = document.getElementById("value4");
    animateValue(obj4, 0, 60000, 1000);


    // scroll animation / career
    $('.career .collapse').on('show.bs.collapse', function () {
        $('html, body').animate({
            scrollTop: $(this).offset().top - 100
        });
    });

    $(document).ready(function() {
        $('.js-default-select').select2({
            allowClear: false,
            theme: 'bootstrap4',
            containerCssClass: "o-box-shadow-base"
        });

        $('.js-default-select-shadow').select2({
            allowClear: true,
            theme: 'bootstrap4',
            containerCssClass: "o-box-shadow-base"
        });

        $('#city').select2({
            theme: 'bootstrap4',
            placeholder: "Hol keresel?",
            allowClear: true,
            multiple: true,
            containerCssClass: "white-select"
        });

        $('#typeOfApartment').select2({
            theme: 'bootstrap4',
            placeholder: "Társasházi lakás",
            allowClear: true,
            multiple: true
        });
        $('#roomNum').select2({
            theme: 'bootstrap4',
            placeholder: "Szobaszám",
            allowClear: true
        });

        $('#buyerContent').select2({
            theme: 'bootstrap4',
            placeholder: "Vásárlói tartalmak érdekelnek",
            allowClear: true
        });

        $('#sorting').select2({
            theme: 'bootstrap4',
        });

        $('#result').select2({
            theme: 'bootstrap4',
        });

        $('#realEstateType').select2({
            theme: 'bootstrap4',
            allowClear: false,
            multiple: true,
            placeholder: "Ingatlan típusa",
            containerCssClass: "white-select o-box-shadow-base"
        });

        $('#houseFlatOption').select2({
            theme: 'bootstrap4',
            placeholder: "Ház / Lakás vásárlás",
            allowClear: true
        });

        $('#contactQuestion').select2({
            theme: 'bootstrap4',
            placeholder: "Tárgy",
            allowClear: true,
            containerCssClass: "white-select o-box-shadow-base"
        });

        $('#contactQuestionS').select2({
            theme: 'bootstrap4',
            placeholder: "Ingatlan típusa",
            allowClear: true,
            containerCssClass: "white-select o-box-shadow-base"
        });

        $('#contactOfficePlace').select2({
            theme: 'bootstrap4',
            placeholder: "Szegedi ingatlaniroda",
            allowClear: true,
            containerCssClass: "white-select o-box-shadow-base"
        });

        $('#contactOfficePlaceS').select2({
            theme: 'bootstrap4',
            placeholder: "Eladom / bérbe adom",
            allowClear: true,
            containerCssClass: "white-select o-box-shadow-base"
        });

        $('#contactOffice').select2({
            theme: 'bootstrap4',
            placeholder: "Ingatlanirodák",
            allowClear: true
        });

        $('#contactOfficeCareer').select2({
            allowClear: true,
            theme: 'bootstrap4',
            placeholder: "Kiválasztott iroda",
            containerCssClass: "white-select o-box-shadow-base"
        });

        $('#contactFinancialNetwork').select2({
            theme: 'bootstrap4',
            placeholder: "Pénzügyi hálózat",
            allowClear: true
        });

        $('#contectOhCenter').select2({
            theme: 'bootstrap4',
            placeholder: "Openhouse Franchise Központ",
            allowClear: true
        });

        $('#office').select2({
            allowClear: true,
            theme: 'bootstrap4',
            placeholder: "Kiválasztott iroda",
            containerCssClass: "white-select o-box-shadow-base"
        });

        $('#tipProperty').select2({
            allowClear: true,
            theme: 'bootstrap4',
            placeholder: "Eladó Ingatlan",
            containerCssClass: "white-select o-box-shadow-base"
        });

        $('#emailNotify').select2({
            allowClear: true,
            theme: 'bootstrap4',
            placeholder: "Azonnal",
            containerCssClass: "white-select o-box-shadow-base"
        });

        $('#crSelect').select2({
            allowClear: true,
            theme: 'bootstrap4',
            placeholder: "Ház / Lakásvásárlás",
            containerCssClass: "white-select o-box-shadow-base"
        });

        $('#salesAgent').select2({
            allowClear: true,
            theme: 'bootstrap4',
            placeholder: "Ingatlan értékesítő",
        });

        $('#salesAgentCity').select2({
            allowClear: true,
            theme: 'bootstrap4',
            placeholder: "Település",
        });

        $('#houseType').select2({
            allowClear: true,
            theme: 'bootstrap4',
            placeholder: "Ház/Lakás/Vásárlás",
            containerCssClass: "white-select o-box-shadow-base"
        });

    });

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $('.carousel-services').slick('setPosition');
            $('#profileDetails .carousel-news-related').slick('setPosition');
        });

        $('.carousel-spec-offers, .carousel-news, .carousel-offices').slick({
            speed: 500,
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000,
            // slickSetOption: true,
            dots:false,
            centerMode: false,
            arrows: true,
            adaptiveHeight: true,
            draggable: true,
            infinite: true,
            responsive: [{
                breakpoint: 1440,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }

            },
                {
                breakpoint: 1360,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
                {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false,
                    infinite: false,
                    autoplay: false,
                    autoplaySpeed: 2000,
                }
            }]
        });

    $('.carousel-services').slick({
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 2000,
        // slickSetOption: true,
        dots:false,
        centerMode: false,
        arrows: true,
        adaptiveHeight: true,
        draggable: true,
        infinite: false,
        responsive: [{
            breakpoint: 1440,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
            }

        },
            {
                breakpoint: 1360,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: false
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false,
                    infinite: false,
                    autoplay: false,
                    autoplaySpeed: 2000,
                }
            }]
    });

        $('.carousel-news-related, .carousel-offers-related').slick({
            speed: 500,
            slidesToShow: 2,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            dots:false,
            centerMode: false,
            arrows: true,
            adaptiveHeight: true,
            responsive: [
                {
                    breakpoint: 1360,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: false,
                        infinite: true,
                        autoplay: true,
                        autoplaySpeed: 2000,
                    }
                }]
        });

        // slick js fix on a tabs
        $('[href="#newsletters"]').on('shown.bs.tab', function (e) {
            $('.carousel-news-related').resize();
        });

        $('.carousel-trust').slick({
            speed: 500,
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            dots:false,
            centerMode: false,
            arrows: true,
            adaptiveHeight: true,
            responsive: [{
                breakpoint: 1440,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }

            },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        dots: false,
                        infinite: true,
                        autoplay: true,
                        autoplaySpeed: 2000,
                    }
                }]
        });

        $('.team-slider').slick({
            speed: 500,
            slidesToShow: 4,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            dots:false,
            centerMode: false,
            arrows: true,
            adaptiveHeight: true,
            responsive: [{
                breakpoint: 1440,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                }

            },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: false,
                        infinite: true,
                        autoplay: true,
                        autoplaySpeed: 2000,
                    }
                }]
        });

        $('.partner-slider').slick({
            speed: 500,
            slidesToShow: 5,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            dots:false,
            centerMode: false,
            arrows: true,
            adaptiveHeight: true,
            responsive: [{
                breakpoint: 1440,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                }

            },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: false,
                        infinite: true,
                        autoplay: true,
                        autoplaySpeed: 2000,
                    }
                }]
        });

        //career
        $('.oh-testi-slider').slick({
            speed: 500,
            slidesToShow: 2,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            dots:false,
            arrows: true,
            adaptiveHeight: true,
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: false,
                        infinite: true,
                        autoplay: true,
                        autoplaySpeed: 2000,
                    }
                }]
        });

        //career
        $('.career-mobile-slider').slick({
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000,
            dots:false,
            arrows: true,
            adaptiveHeight: true
        });

        //career - sales
        $('.sales-slider').slick({
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            dots:false,
            arrows: false,
            adaptiveHeight: true,
            asNavFor: '.sales-nav',
            infinite: true
        });

        $('.sales-nav').slick({
            speed: 500,
            slidesToShow: 1,
            slideToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            arrows: true,
            adaptiveHeight: true,
            asNavFor: '.sales-slider',
            focusOnSelect: true,
            infinite: true,
        });


        $('.carousel-real-estate-list').slick({
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000,
            centerMode: false,
            arrows: false,
            adaptiveHeight: true,
            asNavFor: '.real-estate-nav',
            responsive: [
                {
                breakpoint: 1440,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false,
                    infinite: true,
                    autoplaySpeed: 2000,
                    autoplay: false
                }
            }]
        });

        $('.real-estate-nav').slick({
            speed: 500,
            slidesToShow: 4,
            slideToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000,
            arrows: true,
            adaptiveHeight: true,
            asNavFor: '.carousel-real-estate-list',
            focusOnSelect: true,
            responsive: [{
                breakpoint: 1440,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    dots: false,
                    infinite: true,
                    autoplay: false,
                    autoplaySpeed: 2000,
                    arrows: false,
                }
            }]
        });

        $('.franchise-card-carousel').slick({
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            centerMode: false,
            arrows: true,
            // initialSlide: 2,
            adaptiveHeight: true,
            responsive: [{
                breakpoint: 1440,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                }
            },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: false,
                        infinite: true,
                        autoplay: true,
                        autoplaySpeed: 2000,
                    }
                }]
        });

        $('.financial-flip-slider').slick({
            speed: 500,
            slidesToShow: 5,
            slidesToScroll: 1,
            arrows: false,
            // adaptiveHeight: true,
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: false,
                        infinite: true,
                        autoplay: false,
                        autoplaySpeed: 2000,
                        arrows: true
                    }
                }]
        });

        $('.financial-card-slider').slick({
            speed: 500,
            slidesToShow: 2,
            slidesToScroll: 1,
            arrows: true,
            centerMode: false,
            adaptiveHeight: true,
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        dots: false,
                        infinite: true,
                        autoplay: false,
                        autoplaySpeed: 2000,
                        arrows: true
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: false,
                        infinite: true,
                        autoplay: true,
                        autoplaySpeed: 2000,
                        arrows: true
                    }
                }
            ]
        });

        // real estate slider

        $('.real-estate-slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            cssEase:'linear',
            draggable:false,
            arrows: true,
            fade: false,
            asNavFor: '.r-slider-nav-thumbnails',
        });

        $('.r-slider-nav-thumbnails').slick({
            slidesToShow: 4,
            slidesToScroll: 4,
            asNavFor: '.real-estate-slider',
            dots: true,
            adaptiveHeight: true,
            centerMode:false,
            infinite: false,
            draggable: true,
            focusOnSelect:true,
            arrows: false,
            responsive: [{
                breakpoint: 991,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }
            },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                }]
        });

        $('.icon-slider').slick({
            slidesToShow: 7,
            slidesToScroll: 1,
            dots: false,
            adaptiveHeight: true,
            cssEase:'linear',
            centerMode:false,
            draggable: true,
            focusOnSelect:true,
            arrows: true,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 450,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 360,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                }
            ]
        });

        //homepage
        $(".full-img-slider").slick({
            autoplay: false,
            autoplaySpeed:10000,
            speed:600,
            slidesToShow:1,
            slidesToScroll:1,
            pauseOnHover:false,
            dots: true,
            pauseOnDotsHover:true,
            cssEase:'linear',
            draggable:true,
            arrows: false,
        });

        //franchise
        $(".full-img-slider-2").slick({
            autoplay: false,
            autoplaySpeed:10000,
            speed:600,
            slidesToShow:1,
            slidesToScroll:1,
            pauseOnHover:false,
            dots: false,
            pauseOnDotsHover:true,
            cssEase:'linear',
            draggable:true,
            arrows: false,
            asNavFor: '.full-img-slider-2-nav',
        });

        $('.full-img-slider-2-nav').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: '.full-img-slider-2',
            dots: false,
            adaptiveHeight: true,
            centerMode:false,
            draggable: true,
            focusOnSelect:true,
            arrows: true
            // responsive: [
            //     {
            //         breakpoint: 360,
            //         settings: {
            //             slidesToShow: 3,
            //             slidesToScroll: 1,
            //         }
            //     }
            // ]
        });

        //mini slider slick & modal
        $('.modal-slider-video').slick(
            {
                slidesToShow:1,
                slidesToScroll:1,
                autoplay: true,
                autoplaySpeed: 500,
                speed: 500
            }
        );

        //mini slider slick & modal
        $('.modal-slider-more-images').slick(
            {
                slidesToShow:1,
                slidesToScroll:1,
                autoplay: true,
                autoplaySpeed: 500,
                speed: 500
            }
        );

       //mini slider slick & modal
        $('.modal-slider-mini').slick(
            {
                slidesToShow:1,
                slidesToScroll:1,
                autoplay: true,
                autoplaySpeed: 1000,
                speed: 1000
            }
        );


        $('.collapse').on('shown.bs.collapse', function (e) {
            $('.career-mobile-slider').slick('setPosition');
        });


        $('.modal-slider').on('shown.bs.modal', function (e) {
            $('.modal-slider-mini').slick('setPosition');
            $('.wrap-modal-slider').addClass('open');
        });


        // SHOW PHONE NUMBER ON CLICK

        $('.phone-show, .phone-block').click(function(e){
            // e.preventDefault();
            var mobile = "+36 00 000 000";

            if($('.phone-block').attr('href') == '#') {
                $('.phone-block, .phone-show').attr('href', 'tel:' + mobile).addClass('shown');
                $('.phone-block span').text(mobile);
                $('#side-right .btn.contact-button .bot').text(mobile);
                $('.phone-show').hide();

                return false;
            }
        });

        //reg-login show popup
        $(".login-btn").on("click", function () {
            $("#reg_log_popup").fadeIn();
        });
        $("#reg_log_popup .close-btn").on("click", function () {
            $("#reg_log_popup").fadeOut();
        });



        //career - popup
        $(".career-btn").on("click", function () {
            $("#career_popup").fadeIn( "slow");
        });
        $("#career_popup .close-btn").on("click", function () {
            $("#career_popup").fadeOut( "slow");
        });

        //base tabs
        $("[data-toggle='o-tab']").click(function () {
            var tabs = $(this).attr('data-tabs');
            var tab = $(this).attr("data-tab");
            $(tabs).find(".otab").removeClass("active");
            $(tabs).find(tab).addClass("active");
            $('.otab-menu').removeClass("active");
            $(this).addClass("active");
        });











        //profile details - messages
        $('#actionMenuBtn').click(function(){
            $('.messages-sidebar').toggle();
            $('#messagesViewCol').toggleClass('col-lg-8');
        });


        //profile details - tips delete function
        let fullSection = document.getElementById("statSection");
        let deleteTips = document.getElementById("tipDelete");
        if (deleteTips) {
            deleteTips.onclick = function () {
                fullSection.parentNode.removeChild(fullSection);
            };
        }

        //Back to top button
        var backToTopButton = $('#backToTopButton');

        $(window).scroll(function() {
            if ($(window).scrollTop() > 300) {
                backToTopButton.addClass('show');
            } else {
                backToTopButton.removeClass('show');
            }
        });

        backToTopButton.on('click', function(e) {
            e.preventDefault();
            $('html, body').animate({scrollTop:0}, '300');
        });

        var $el, $ps, $up, totalHeight;

        $(".show-more-box .show-more-btn").click(function() {

            totalHeight = 0

            $el = $(this);
            var $p  = $el.parent();
            $up = $p.parent();
            $ps = $up.find("p:not('.read-more')");

            // measure how tall inside should be by adding together heights of all inside paragraphs (except read-more paragraph)
            $ps.each(function() {
                totalHeight += $(this).outerHeight() + 60;
            });

            $up
                .css({
                    // Set height to prevent instant jumpdown when max height is removed
                    "height": $up.height(),
                    "max-height": 9999
                })
                .animate({
                    "height": totalHeight
                });

            // fade out read-more
            $p.fadeOut();

            // prevent jump-down
            return false;

    });

});

}