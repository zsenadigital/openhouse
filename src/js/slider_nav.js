export default function () { }
window.jQuery = window.$ = require("jquery");

(function ($) {
    $(".real-estate-nav p").each(function (i, item) {
        item.onclick = function (event) {
            var _this = $(event.target);

            $(".real-estate-nav p").removeClass("active");
            _this.addClass("active");
        }
    });

})(jQuery);