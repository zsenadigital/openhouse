export default function () {
}

window.jQuery = window.$ = require('jquery');


(function () {
    "use strict";
    let clearBtn = document.getElementById("clearFields");
    (function () {
        if (clearBtn) {
            clearBtn.onclick = function () {

                var x = document.querySelectorAll(".reset");
                x.forEach(el => {
                    el.value = '';
                });

                document.getElementById('userMessageP').innerHTML = '';
            };
        }
    })();

})(jQuery);