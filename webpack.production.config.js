const CleanWebpackPlugin      = require('clean-webpack-plugin');
const defaultConfig           = require('./webpack.config.js');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const Path                    = require('path');


var config = defaultConfig;

config.devtool = false;
config.mode = 'production';
config.optimization = {
    minimizer: [
        new OptimizeCSSAssetsPlugin(),
    ]
};

module.exports = config;